package br.com.itau.Cliente.Service;


import br.com.itau.Cliente.Exception.ClienteNotFoundException;
import br.com.itau.Cliente.Models.Cliente;
import br.com.itau.Cliente.Repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService
{
    @Autowired
    private ClienteRepository clienteRepository;

    //*** Cliente Novo
    public Cliente criar (Cliente cliente)
    {

        return  clienteRepository.save(cliente);
    }

  //**** Pesquisa Cliente por ID
    public Cliente buscarClientePorId(int id)
    {
        Optional<Cliente> clienteOptional = clienteRepository.findById(id);

        if(clienteOptional.isPresent())
        {
            Cliente cliente = clienteOptional.get();
            return  cliente;
        }
        else
        {
            throw new ClienteNotFoundException();
        }
    }
}
